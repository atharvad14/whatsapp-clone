import React from 'react';
import './App.css';
import Sidebar from './Sidebar';
import Chat from './Chat';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './Login';
import { useStateValue } from './StateProvider';
import BlankChat from './BlankChat';


function App() {
  const [{ user },dispatch] = useStateValue();
  // const [user,setUser] = useState(null);
  return (
    <div className="app" >
      {user? (
         <div className="appBody">
        

         <Router>
           {/* Sidebar */}
         <div className="sidebar">
           <Sidebar />
         </div>
           <Switch>
             <Route path="/chats/:chatId">
 
 
               {/* Chat */}
               <div className="chat">
                 <Chat />
               </div>
             </Route>
             <Route path="/">
              <BlankChat/>
             </Route>
           </Switch>
         </Router>
 
 
       </div>
      ):(
        <Login></Login>
      )}
     
    </div>
  );
}

export default App;
