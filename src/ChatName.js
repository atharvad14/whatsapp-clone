import React, { useEffect, useState } from 'react'
import { Avatar } from '@material-ui/core'
import './ChatName.css';
import { Link } from 'react-router-dom';
import db from './firebase';
function ChatName({id,key,name}) {
    const [messages,setMessages] = useState([]);
    useEffect (()=> {
        if (id) {
            db.collection('chats')
                .doc(id).collection("messages")
                    .orderBy("timestamp","desc")
                        .onSnapshot((snapshot) => setMessages(snapshot.docs.map((doc) => doc.data())))
        }
    },[id])
    var lastMessage = (messages[0]?.message);
    
    return (
        
       <Link className = "myLink" to={`/chats/${id}`}>
             <div className="chatName">
            <div className="chatAvatar">
                <Avatar/>
            </div>
            <div className="chatInfo">
                <h3>{name}</h3>
                <p>
                {
                    lastMessage
                }
                </p>
               
            </div>
        </div>
        </Link>

       
    )
}

export default ChatName
