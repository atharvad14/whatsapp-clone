import React from 'react'
import './Chat.css'
import { Avatar } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';

import SendIcon from '@material-ui/icons/Send';

function BlankChat() {
    return (
        <div className="chat">
        <div className="chatHeader">
            <div className="chatHeaderLeft">
                <Avatar />
                {/* <h4>{chatName}</h4> */}
            </div>
            <div className="chatHeaderRight">
                <SearchIcon />
                <AttachFileIcon />
                <MoreVertIcon />
            </div>

        </div>
        <div className="chatBodyBlank">
           
           <h1>Select a chat to get started</h1>
            
           
        </div>
        <div className="chatFooter">

            <EmojiEmotionsIcon />
            <form disabled> 
                <input value= ""  type='text' className="chatSearchBox" placeholder="Type a Message" disabled/>
                <button className="formButton" disabled><SendIcon/></button>
            </form>

        </div>

    </div>
    )
}

export default BlankChat

