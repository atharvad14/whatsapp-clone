import React, { useState, useEffect } from 'react'
import './Chat.css'
import { Avatar } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';

import SendIcon from '@material-ui/icons/Send';
import Message from './Message';
import { useParams } from 'react-router-dom';
import db from './firebase';
import firebase from 'firebase';
import { useStateValue } from './StateProvider';


function Chat() {
    const [input, setInput] = useState("");
    const {chatId} = useParams();
    const [chatName,setChatName]=useState("");
    const [messages,setMessages] = useState([]);
    const [{ user },dispatch]  = useStateValue();
    useEffect (()=> {
        if (chatId)
        {
            db.collection("chats").doc(chatId).onSnapshot((snapshot)=> setChatName(snapshot.data().Name))
            db.collection('chats').doc(chatId).collection("messages").orderBy('timestamp','asc').onSnapshot(snapshot => (
                setMessages(snapshot.docs.map((doc) => doc.data()))
            ))
        }
    },[chatId])
    const sendMessage = (e) => {
        e.preventDefault();
        if (input !== '')
        {
        
        setInput('')

        db.collection("chats").doc(chatId).collection('messages').add({
            message: input,
            name: user.displayName,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            email: user.email,
        })
    }
        
    };
    return (
        <div className="chat">
            <div className="chatHeader">
                <div className="chatHeaderLeft">
                    <Avatar />
                    <h4>{chatName}</h4>
                </div>
                <div className="chatHeaderRight">
                    <SearchIcon />
                    <AttachFileIcon />
                    <MoreVertIcon />
                </div>

            </div>
            <div className="chatBody">
                {messages.map(message=> (
                    <Message message = {message}/>
                ))}
                
               
            </div>
            <div className="chatFooter">

                <EmojiEmotionsIcon />
                <form>
                    <input value={input} onChange={
                        e => setInput(e.target.value)
                    } type='text' className="chatSearchBox" placeholder="Type a Message" />
                    <button className = "formButton" type="submit" onClick={sendMessage}><SendIcon/></button>
                </form>

            </div>

        </div>
    )
}

export default Chat
