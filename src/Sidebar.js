import React, { useState,useEffect } from 'react';
import './Sidebar.css';
// import SettingsIcon from '@material-ui/icons/Settings';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ChatIcon from '@material-ui/icons/Chat';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SearchIcon from '@material-ui/icons/Search';

import { Avatar } from '@material-ui/core';
import ChatName from './ChatName';

import db from './firebase'
import { useStateValue } from './StateProvider';


function Sidebar() {
    const [chats,setChats] =  useState([]);
    const [{ user },dispatch] = useStateValue();
    useEffect(() => {
        const unsubscribe = db.collection("chats").onSnapshot(snapshot => (
            setChats(snapshot.docs.map(doc => (
                {
                    id: doc.id,
                    data: doc.data()
                }
            )))
        ))
        return () => {
            unsubscribe();
        }

    },[]); 
    const addNewChat = () =>  {
        const chatName = prompt("Please Enter Name for chat room");
        if (chatName) {
            db.collection("chats").add({Name: chatName});
        }
    }
    return (
        <div className = "sidebar">
            <div className="sidebarHeader">
                <div className = "sidebarHeaderLeft">
                     <Avatar src = {user?.photoURL}/>
                </div>
                <div className = "sidebarHeaderRight">
                    <DonutLargeIcon />
                    <ChatIcon />
                    <MoreVertIcon/>
                </div>
                

            </div>
            <div className="sidebarSearch">
                <SearchIcon className = "searchIcon"/>
                <input type = 'text' className = "searchBox" placeholder = "Search or Start New Chat" />
                
               
            </div>
            <h1 type = "button" className = "sidebarAdd" onClick = {addNewChat}>Add New Chat</h1>
            <div className="sidebarChats">
                {
                    chats.map(chat => (
                        <ChatName key = {chat.id} id = {chat.id} name = {chat.data.Name} />
                    ))
                }
                
                {/* <ChatName/>
                <ChatName/>
                <ChatName/>
                <ChatName/>
                <ChatName/>
                <ChatName/> */}
            </div>
        </div>
    )
}

export default Sidebar
