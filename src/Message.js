import React from 'react';
import './Message.css';
import { useStateValue } from './StateProvider';
function Message({message}) {
    const [{ user },dispatch] = useStateValue();
    var choice;
    if (message.email == user.email)
    {
        choice = true;
    }
    else {
        choice = false;
    }
    
    return (
        <p className = {` ${choice && 'messageRight'} message`}>
            <h5 className = "messageSender">{message.name}</h5>
            <div className = {`${choice && 'myMessageDataRight'} ${true && 'myMessageDataLeft'}  `}>
                {message.message}
                <span className = "messageTime">
                    {new Date(message.timestamp?.toDate()).toUTCString()}
                    
                </span>
            </div>
            <br/>
        </p>
    )
}

export default Message
